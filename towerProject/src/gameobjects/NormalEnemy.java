package gameobjects;

import map.*;

import java.util.ArrayList;

public class NormalEnemy extends Enemy {


    public NormalEnemy(ArrayList<Floor> road, int enemyPoints) {
        super(road, EnemyTypes.NORMAL.getColor(), EnemyTypes.NORMAL,1, 40, 10);
    }

}
