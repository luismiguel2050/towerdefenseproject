public class Player {

    private String name;
    private int gold;
private int score;
    private int health;

    public Player(String name){
        this.name=name;
        this.gold=100;
        this.health=5;
        this.score=0;
    }

    public String getName() {
        return name;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    public int getHealth(){
        return health;
    }
    public void setHealth(int health){
        this.health = health;
    }

    public int getScore(){
        return score;
    }
    public void setScore(int score){
        this.score= score;
    }
}
