package map;

import org.academiadecodigo.simplegraphics.graphics.*;

public class Button {

    private Rectangle background, border;
    private String text;
    private boolean isPressed = false;
    private Text label;

    private Color backgroundColor,borderColor,textColor;
    private int col,row,targetCol,targetRow;

    public Button(int col,int row,int targetCol,int targetRow,Color backgroundColor,Color borderColor, String text,Color textColor)
    {
        this.col = (col * Map.CELLSIZE)+ Map.PADDING;
        this.row = (row * Map.CELLSIZE)+ Map.PADDING;
        this.targetCol = (targetCol * Map.CELLSIZE);
        this.targetRow = (targetRow * Map.CELLSIZE);


        background = new Rectangle(this.col,this.row,this.targetCol,this.targetRow);
        border = new Rectangle(this.col,this.row,this.targetCol,this.targetRow);
        background.setColor(backgroundColor);
        border.setColor(borderColor);
        int centerX = this.col+(this.targetCol/2)-Map.PADDING;
        int centerY = this.row+(this.targetRow/2)-Map.PADDING;
        label = new Text(centerX,centerY,text);
        label.grow(this.targetCol/3,this.targetRow/3);
        label.setColor(textColor);


    }
    public void drawButton()
    {
        background.fill();
        border.draw();
        label.draw();
    }

    public boolean isClicked(int x, int y) {
        if(((x > col) && (x < col + targetCol)
                && (y > row) && (y < row + targetRow))&&!isPressed)
        {
            isPressed = true;
            return true;
        }
        return false;
    }

    public void buttonDelete()
    {
        background.delete();
        border.delete();
        label.delete();
    }
    public void notPressed()
    {
        isPressed = false;
    }
}
