import gameobjects.Enemy;
import gameobjects.NormalEnemy;
import map.Map;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class EnemySpawner {
    private Map map;
    private ArrayList<Enemy> enemies;
    private int nrEnemies;

    private boolean forceSpawn = false;
    Timer timer;
    private int delay = 5000;
        public EnemySpawner(Map map, ArrayList<Enemy> enemies, int nrEnemies)
        {
            this.enemies = enemies;
            this.map  = map;
            this.nrEnemies = nrEnemies;
        }

    public void startSpawn()
    {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                    spawn();
            }
        },0,delay);
    }
    public void spawn()
    {
        synchronized (enemies) {
            if(nrEnemies>0) {
                Enemy enemy = new NormalEnemy(map.getRoadsArray(), 40);
                enemies.add(enemy);
                nrEnemies--;
            }
        }

    }
    public void nextWave()
    {
        if(nrEnemies ==0) {
            System.out.println("Next Wave");
            nrEnemies = 5;
        }
    }



}
