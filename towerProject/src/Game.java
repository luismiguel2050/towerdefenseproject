import gameobjects.Enemy;
import gameobjects.Tower;

import map.Button;
import map.Floor;
import map.Position;
import map.Map;


import org.academiadecodigo.simplegraphics.graphics.Canvas;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

public class Game implements MouseListener {

    private Map map;
    public static final int BORDERS_Y = 24;
    public static final int BORDERS_X = 0;
    private Rectangle background;
    private Text title;
    private Button start;
    private Button spawnButton;
    private boolean gameOver;

    private ArrayList<Tower> towers = new ArrayList<>();
    private ArrayList<Enemy> enemies = new ArrayList<>();

    private EnemySpawner spawner;
    private InfoPanel infoPanel;




    public Game() {
        introduction();
        gameOver = false;
    }

    //Cria o menu inicial, ainda vai ser editado
    public void introduction() {
        background = new Rectangle(Map.PADDING, Map.PADDING, Map.WIDTH, Map.HEIGHT+Map.CELLSIZE);
        background.setColor(Color.GRAY);
        background.fill();
        title = new Text(Map.WIDTH / 2, Map.HEIGHT / 4, "Tower Defense");
        title.grow(200, 150);
        title.setColor(Color.YELLOW);
        title.draw();
        start = new Button(8, 10, 5, 2, Color.DARK_GRAY, Color.BLACK, "Start", Color.RED);
        start.drawButton();
        spawnButton = new Button(1,1,1,1,Color.GRAY,Color.BLACK,"+",Color.RED);
        Canvas.getInstance().addMouseListener(this);
    }

    public void drawMap() throws FileNotFoundException {
        background.delete();
        title.delete();
        map = new Map();
        map.showMap();
        spawnButton.drawButton();
        infoPanel = new InfoPanel();
        infoPanel.displayUI();

    }

    public void gameOver()
    {
        gameOver = true;
        Rectangle rectangle = new Rectangle(Map.PADDING,Map.PADDING,Map.WIDTH,Map.HEIGHT+Map.CELLSIZE);
        rectangle.fill();
        Text gameO = new Text(Map.WIDTH / 2, Map.HEIGHT / 2, "GAME OVER!");
        gameO.grow(300, 150);
        gameO.setColor(Color.RED);
        gameO.draw();

    }

    public void startGame() throws FileNotFoundException {
        drawMap();

        synchronized (enemies) {
            spawner = new EnemySpawner(map, enemies, 5);
        }
        spawner.startSpawn();


        Thread enemyThread = new Thread(this::enemyUpdate);
        Thread towerThread = new Thread(this::towerUpdate);
        enemyThread.start();
        towerThread.start();
    }

    private void enemyUpdate() {
        while (!gameOver) {
                synchronized (enemies) {
                    Iterator<Enemy> iterator = enemies.iterator();
                    while (iterator.hasNext()) {
                        Enemy enemy = iterator.next();
                        enemy.move();
                        if(enemy.isEnemyWon()){
                            infoPanel.looseHealth();
                            iterator.remove();
                            if(infoPanel.isDead())
                            {
                                gameOver();
                            }
                        }

                        if (enemy.isDead()) {
                                infoPanel.setScore(infoPanel.getNrScore()+enemy.getEnemyPoints());
                                infoPanel.setGold(infoPanel.getNrGold()+enemy.getEnemyPoints());
                            iterator.remove();
                        }
                    }
                }
            try {
                Thread.sleep(13);
            } catch (Exception e) {
                throw new IllegalThreadStateException();
            }
        }
    }

    private void towerUpdate() {
        while (!gameOver) {
            synchronized (towers) {
                for (Tower tower : towers) {
                    tower.shoot(enemies);
                }
            }
            try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    throw new IllegalThreadStateException();
                }
            }
        }

    //add Nested array with block tiles
    //alterar



    // IMCOMPLETOOOOOOOOOOOO
    public  synchronized Tower generateTower(Position position, ArrayList<Floor> floorsArray) {
        synchronized (towers) {
            Tower tower = null;
            boolean canConstruct = true;
            for (Floor floor : floorsArray) {
                if (floor != null && position.isEqual(floor.getPosition())) {
                    if (towers.isEmpty()) {
                        tower = new Tower(position);
                        towers.add(tower);
                        infoPanel.setGold(infoPanel.getNrGold() - Tower.PRICE);
                    } else {
                        for (Tower t : towers) {
                            if (t.getPosition().equals(position) || infoPanel.getNrGold()< Tower.PRICE) {
                                canConstruct = false;
                            }
                        }
                            if (canConstruct) {

                                tower = new Tower(position);
                                towers.add(tower);
                                infoPanel.setGold(infoPanel.getNrGold() - Tower.PRICE);
                            }

                        }
                    }


            }
            return tower;
        }
    }




    // IMCOMPLETOOOOOOOOOOOO
    //Diogo: mudei o BORDERS_X e Y, porque as coordenadas nao estavam certas
    //mudei a funcao map.xToColumn e map.yToRow que estavam com a formula errada
    //movi as funcoes para mousePressedd Funciona melhor!
    @Override
    public void mouseClicked(MouseEvent event) {
    }


    @Override
    public void mousePressed(MouseEvent event) {
        int mouseX = event.getX() - BORDERS_X;
        int mouseY = event.getY() - BORDERS_Y;
        //System.out.println(mouseX + " " + mouseY);


        if(start.isClicked(mouseX,mouseY)) {
                start.buttonDelete();
            try {
                startGame();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        if(map != null) {
            int clickedX = map.xToColumn(mouseX-Map.PADDING);
            int clickedY = map.yToRow(mouseY-Map.PADDING);

            //System.out.println((mouseX-Map.PADDING) + " "+ (mouseY-Map.PADDING));
            //System.out.println("x- " + clickedX + " " + "y- " + clickedY);
            //Diogo: estam a criar torres no constructsArray?
            if(spawnButton.isClicked(mouseX,mouseY))
            {
                spawner.nextWave();
                spawnButton.notPressed();
            }

            generateTower(new Position(clickedX, clickedY), map.getConstructsArray());
        }

    }

    public void getTileType(Position pos, Floor[] floorsArray){
        for(Floor floor : floorsArray){
            if(floor != null && pos.isEqual(floor.getPosition())){
                System.out.println(floor.getFloorType());
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
